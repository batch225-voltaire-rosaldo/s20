// 3.Create a variable number that will store the value of the number provided by the user via the prompt.
// 4.Create a for loop that will be initialized with the number provided by the user, will stop when the value is less than or equal to 0 and will decrease by 1 every iteration.
// 5.Create a condition that if the current value is less than or equal to 50, stop the loop.
// 6.Create another condition that if the current value is divisible by 10, print a message that the number is being skipped and continue to the next iteration of the loop.
// 7.Create another condition that if the current value is divisible by 5, print the number.


let count1 = Number(prompt("Enter your number: "));
console.log("The number you provided is " + count1);
while(count1 > 50){

	if (count1 % 10 == 0) {
		console.log("The number is divisible by 10 Skipping the number ");
	} else if (count1 % 5 == 0) {
		console.log(count1);
	}

	count1 --;

}
console.log("The number is at " + count1 + " Terminate the loop.");

let myName = "supercalifragilisticexpialidocious";
function removeVowel( input ){

    return input.replace(/[aieou]/gi, "");
}
console.log("Your word: " + myName);
console.log("Consonants are: " + removeVowel(myName));

function isAlphabet(ch) {
        if (ch >= "a" && ch <= "z") return true;
        if (ch >= "A" && ch <= "Z") return true;
 
        return false;
} 
// Function to return the string after
// removing all the consonants from it
function remConsonants(str) {
  	var vowels =
  	["a", "e", "i", "o", "u", "A", "E", "I", "O", "U"];

  	var sb = "";

  	for (var i = 0; i < str.length; i++) {
  		var present = false;
  		for (var j = 0; j < vowels.length; j++) {
  			if (str[i] === vowels[j]) {
  				present = true;
  				break;
  			}
  		}

  		if (!isAlphabet(str[i]) || present) {
  			sb += str[i];
  		}
  	}
  	return sb;
}

console.log("Vowels are: " + remConsonants(myName));












