// [SECTION] While Loop

/*
	Syntax
	while (expression/condition){
		statement
	}
*/

// ++ - iteration
let count = 0;

while(count <= 10){
	//console.log("While: " + count);

	count ++;
}

// -- iteration
let count1 = parseInt(prompt("Enter: "));
// let count1 = 10;

while(count1 >= 0){
	console.log("While: " + count1);

	count1 --;
}

// A do-while loop works a lot like the while loop. But unlike while loop, do-while loops guarantee that the code will be executed at least once.

/*
	Syntax:
	do {
		statement
	} while (expression/condition)
*/

// let number = Number(prompt("Give me a number"));

// do {

	//console.log("Do while: " + number);
	// number += 1 ;
// } while (number < 10)

// [SECTION] For loops

// A for loop is more flexible than while and do-while loop.

/*
	Syntax
	for (initialization; expression/condition; finalExpression) {
		statement
	}
*/

for (let count = 20; count <= 0; count--) {
	// console.log(count);
}


// for (let x = 0) {

// }

//console.log(myString[x])


// let myName = "DelIZo";

// for (let i = 0; i < myName.length; i++) {
// 	if (
// 		myName[i].toLowerCase() == "a" || 
// 		myName[i].toLowerCase() == "e" || 
// 		myName[i].toLowerCase() == "i" || 
// 		myName[i].toLowerCase() == "o" || 
// 		myName[i].toLowerCase() == "u" 
// 	){
// 		console.log(3);
// 	} else {
// 		console.log(myName[i]);
// 	}
// }


// let myName = "supercalifragilisticexpialidocious";

// for (let i = 0; i < myName.length; i++) {
// 	if (
// 		myName[i].toLowerCase() == "a" || 
// 		myName[i].toLowerCase() == "e" || 
// 		myName[i].toLowerCase() == "i" || 
// 		myName[i].toLowerCase() == "o" || 
// 		myName[i].toLowerCase() == "u" 
// 	){
// 		console.log("");
// 	} else {
// 		console.log(myName[i]);
// 	}
// }





